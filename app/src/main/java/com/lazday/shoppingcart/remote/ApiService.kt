package com.lazday.shoppingcart.remote

import com.lazday.shoppingcart.shopping.ProductModel
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @GET("products")
    fun getProducts(): Call<List<ProductModel>>
}