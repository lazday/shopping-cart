package com.lazday.shoppingcart.persistence

import android.content.Context
import androidx.room.Room

object DbClient{
    fun getService( context: Context): DbService {
        return Room.databaseBuilder(
            context,
            DbService::class.java,
            "shoppingCart.db"
        ).build()
    }
}