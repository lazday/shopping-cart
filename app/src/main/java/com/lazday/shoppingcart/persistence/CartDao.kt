package com.lazday.shoppingcart.persistence

import androidx.lifecycle.LiveData
import androidx.room.*
import com.lazday.shoppingcart.cart.CartModel

@Dao
interface CartDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(cartModel: CartModel)

    @Update
    fun update(cartModel: CartModel)

    @Delete
    fun delete(cartModel: CartModel)

    @Query("SELECT * FROM tableCart")
    fun carts(): LiveData<List<CartModel>>

    @Query("SELECT * FROM tableCart WHERE title=:title")
    fun carts(title: String): CartModel?

    @Query("DELETE FROM tableCart")
    fun deleteAll()
}
