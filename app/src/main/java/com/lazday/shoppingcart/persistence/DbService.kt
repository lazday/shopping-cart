package com.lazday.shoppingcart.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import com.lazday.shoppingcart.cart.CartModel

@Database(
    entities = [CartModel::class],
    exportSchema = false,
    version = 1
)
abstract class DbService: RoomDatabase() {
    abstract fun cartDao(): CartDao
}

