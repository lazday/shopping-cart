package com.lazday.shoppingcart.cart

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lazday.shoppingcart.R
import com.lazday.shoppingcart.databinding.AdapterCartBinding
import com.squareup.picasso.Picasso

class CartAdapter (
    var carts: ArrayList<CartModel>,
    var listener: OnAdapterListener?
): RecyclerView.Adapter<CartAdapter.ViewHolder>(){

    fun submitList(newList: List<CartModel>) {
        if (carts.isNotEmpty()) carts.clear()
        carts.addAll(newList)
        notifyDataSetChanged()
    }

    class ViewHolder(val binding: AdapterCartBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(cart: CartModel, listener: OnAdapterListener?) {
            binding.textTitle.text = cart.title
            binding.textPrice.text = "$" + cart.price.toString()
            binding.textSubtotal.text = "$" + (cart.price * cart.qty) .toString()
            binding.textQty.text = "x${cart.qty}"
            Picasso.get()
                .load(cart.image)
                .placeholder(R.drawable.ic_image)
                .fit().centerCrop()
                .into(binding.imgProduct)
            binding.btnPlus.setOnClickListener {
                listener?.plus( cart )
            }
            binding.btnMinus.setOnClickListener {
                listener?.minus( cart )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder (
        AdapterCartBinding
            .inflate(LayoutInflater.from( parent.context), parent, false)
    )

    override fun getItemCount() = carts.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(carts[position], listener)
    }

    interface OnAdapterListener {
        fun plus(cart: CartModel)
        fun minus(cart: CartModel)
    }
}