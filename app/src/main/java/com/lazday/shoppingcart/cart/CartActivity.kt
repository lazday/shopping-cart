package com.lazday.shoppingcart.cart

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.lazday.shoppingcart.databinding.ActivityCartBinding
import com.lazday.shoppingcart.persistence.DbClient

class CartActivity : AppCompatActivity() {

    private val TAG = CartActivity::class.java.simpleName.toString()
    private val binding by lazy { ActivityCartBinding.inflate(layoutInflater) }
    private val db by lazy { DbClient.getService( this ).cartDao() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        supportActionBar!!.title = "Cart"
        binding.listCart.adapter = cartAdapter

        db.carts().observe(this, {
            Log.e(TAG, it.toString())
            cartAdapter.submitList( it )

            var subtotal = 0.0
            it.forEach {  cart ->
               subtotal += cart.price * cart.qty
            }
            binding.textTotal.text = "$" + subtotal.toString()
        })
    }

    private val cartAdapter by lazy {
        CartAdapter(arrayListOf(), object : CartAdapter.OnAdapterListener {
            override fun plus(cart: CartModel) {
                cart.qty += 1
                Thread {
                    db.update( cart )
                }.start()
            }
            override fun minus(cart: CartModel) {
                if (cart.qty <= 1) {
                    Thread { db.delete( cart ) }.start()
                } else {
                    cart.qty -= 1
                    Thread { db.update( cart ) }.start()
                }
            }
        })
    }
}