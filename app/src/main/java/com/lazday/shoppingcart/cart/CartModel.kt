package com.lazday.shoppingcart.cart

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tableCart")
data class CartModel (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    var title: String,
    var image: String,
    var price: Double,
    var qty: Int,
)