package com.lazday.shoppingcart.shopping

import com.google.gson.annotations.SerializedName

data class ProductModel (
    @SerializedName("id")
    val id: Int?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("price")
    val price: Double?,
    @SerializedName("image")
    val image: String?
)