package com.lazday.shoppingcart.shopping

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.android.material.snackbar.Snackbar
import com.lazday.shoppingcart.cart.CartActivity
import com.lazday.shoppingcart.cart.CartModel
import com.lazday.shoppingcart.databinding.ActivityShoppingBinding
import com.lazday.shoppingcart.persistence.DbClient
import com.lazday.shoppingcart.remote.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ShoppingActivity : AppCompatActivity() {

    private val TAG = ShoppingActivity::class.java.simpleName.toString()
    private val binding by lazy { ActivityShoppingBinding.inflate(layoutInflater) }
    private val api by lazy { ApiClient.service }
    private val db by lazy { DbClient.getService( this ).cartDao() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        supportActionBar!!.title = "Shopping"
        binding.listProduct.adapter = shoppingAdapter

        getProducts()
        binding.swipe.setOnRefreshListener {
            getProducts()
        }

        db.carts().observe(this, {
            binding.fabCart.visibility = if (it.isEmpty()) View.GONE else View.VISIBLE
        })

        binding.fabCart.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }
    }

    private val shoppingAdapter by lazy {
        ShoppingAdapter(arrayListOf(), object : ShoppingAdapter.OnAdapterListener {
            override fun addToCart(product: ProductModel) {
                addCart( product )
            }
        })
    }

    private fun addCart(product: ProductModel) {

        val cart: CartModel =
            CartModel (
                id = 0,
                title = product.title!!,
                image = product.image!!,
                price = product.price!!,
                qty = 1
            )

        Thread {
            val checkCart: CartModel? = db.carts( product.title )
            runOnUiThread {

                Thread {
                    if (checkCart != null && checkCart.title == product.title) {
                        checkCart.qty = checkCart.qty + 1
                        db.update( checkCart )
                    } else {
                        db.insert( cart )
                    }
                    runOnUiThread {
                        Snackbar.make(
                            binding.fabCart,
                            "Added To Cart",
                            Snackbar.LENGTH_LONG
                        ).show()
                    }
                }.start()
                
            }
        }.start()
    }

    private fun getProducts(){
        binding.swipe.isRefreshing = true
        api.getProducts().enqueue(object : Callback<List<ProductModel>> {
            override fun onResponse(
                call: Call<List<ProductModel>>,
                response: Response<List<ProductModel>>
            ) {
                val products: List<ProductModel> = response.body()!!
                Log.e(TAG, products.toString())

                shoppingAdapter.submitList( products )
                binding.swipe.isRefreshing = false
            }

            override fun onFailure(call: Call<List<ProductModel>>, t: Throwable) {
                Log.e(TAG, t.toString())
                binding.swipe.isRefreshing = false
            }

        })
    }
}