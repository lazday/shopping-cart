package com.lazday.shoppingcart.shopping

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lazday.shoppingcart.R
import com.lazday.shoppingcart.databinding.AdapterShoppingBinding
import com.squareup.picasso.Picasso

class ShoppingAdapter (
    var products: ArrayList<ProductModel>,
    var listener: OnAdapterListener?
): RecyclerView.Adapter<ShoppingAdapter.ViewHolder>(){

    fun submitList(newList: List<ProductModel>) {
        if (products.isNotEmpty()) products.clear()
        products.addAll(newList)
        notifyDataSetChanged()
    }

    class ViewHolder(val binding: AdapterShoppingBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(product: ProductModel, listener: OnAdapterListener?) {
            binding.textTitle.text = product.title
            binding.textPrice.text = "$" + product.price.toString()
            Picasso.get()
                .load(product.image)
                .placeholder(R.drawable.ic_image)
                .fit().centerCrop()
                .into(binding.imgProduct)
            binding.btnAddToCart.setOnClickListener {
                listener?.addToCart( product )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder (
        AdapterShoppingBinding
            .inflate(LayoutInflater.from( parent.context), parent, false)
    )

    override fun getItemCount() = products.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(products[position], listener)
    }

    interface OnAdapterListener {
        fun addToCart(product: ProductModel)
    }
}