# Shopping Cart

## Tech Stack
- Kotlin Android
- Api (products) https://fakestoreapi.com/
- XML
- Retrofit
- Room Persistence
- Etc

## Final project

- [Download Apk](https://bitbucket.org/lazday/shopping-cart/src/master/apk/) 

![Screenshot](https://bitbucket.org/lazday/shopping-cart/raw/6618b7434a8ca47f0d14d28e8eb10e2bf296bb75/screenshot/device-2021-10-23-220011.png)